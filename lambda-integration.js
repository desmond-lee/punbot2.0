var http = require('http')
var https = require('https')
var queryString = require('querystring');

// token to control punbot TODO: move this into s3 and out of git for best security practices
var bot_token = "xoxb-98126216466-RGq6h3Owz3ZAyAiMRKDXo7Pb"

exports.handler = (event, context, callback) => {
  console.log("Received event for punbot:", event)

  if (event.source == "aws.events" && event['detail-type'] == "Scheduled Event") {
      getPunOfTheDay(function(err, pun) {
        if (!err) {
          sendMessage("Here's the pun of the day: " + pun)
        }
      })
  }
  // check request token, this verifies that our slack integration made the request
  else if (event.token == "DwU2NoXCCqI3Rhvb4yweFhi5") {
    var username = event['user_name']
    var text = event['text']


    var matchPunOfTheDayRequest = [
      /what('|’)?s the pun of the day/i,
      /test pun please ignore/i
    ]
    .map(function(r){return r.test(text)})
    .reduce(function(a, b){return a || b}, false);

    var matchSpecificPun = [
      /give me a pun about .*/i
    ]
    .map(function(r){return r.test(text)})
    .reduce(function(a, b){return a || b}, false);

    if (matchPunOfTheDayRequest) {
      getPunOfTheDay(function(err, pun) {
        if (!err) {
          sendMessage("Hey, " + username + ", here is your pun of the day: " + pun, callback);
        } else {
            callback(err)
        }
      })
    } else if (matchSpecificPun) {
      var l = text.toLowerCase().indexOf("give me a pun about ");
      var keyword = text.substring(l + 20).replace(/\W/g, '');
      getSpecificPun(keyword, function(err, pun) {
        if (!err) {
          if (pun) {
            sendMessage("Hey, " + username + ", here is your pun about " + keyword + ": " + pun, callback);
          } else {
            sendMessage("Sorry " + username + ", I couldn't find a pun about " + keyword + ".", callback);
          }
        } else {
          callback(err)
        }
      })
    }

  } else {
    callback("Bad token")
  }
}

function sendMessage(message, callback) {
  var query = {
    token: bot_token,
    channel: "#punspunspuns",
    text: message,
    as_user: true
  };

  console.log("making postMessage request");

  https.get("https://slack.com/api/chat.postMessage?" + queryString.stringify(query), function(res) {
    console.log("Got response from slack: ", res);
  }).on('error', function(e) {
    console.log("Got error: " + e.message);
  }).on('end', function() {
    console.log("got response");
    callback()
  })
}

function getSpecificPun(keyword, callback) {
  var url = "http://www.punoftheday.com/cgi-bin/findpuns.pl?q=" + keyword + "&submit=Search";

  http.get(url, function (http_res) {
    // initialize the container for our data
    var data = "";

    // this event fires many times, each time collecting another piece of the response
    http_res.on("data", function (chunk) {
      // append this chunk to our growing `data` var
      data += chunk;
    });

    // this event fires *one* time, after all the `data` events/chunks have been gathered
    http_res.on("end", function () {
      console.log('done getting the pun of the day');
      // you can use res.send instead of console.log to output via express
      // console.log(data);
      var table = "<table class=\"pundisplay\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
      var tableEnd = "</table>"
      var tableContents = data.substring(data.indexOf(table) + table.length, data.indexOf(tableEnd));

      var puns = tableContents.replace(/<[^>]+>/g, '').replace(/\d.*/g, '').replace(/\n\n/g, '\n')
          .split("\n").filter(function(r){return r.length})

      if (puns.length == 0) {
        callback(null, null)
      } else {
        var pun = puns[Math.floor(puns.length * Math.random())]
        callback(null, pun);
      }
    });
  });
}

function getPunOfTheDay(callback) {
  var options = {
    host: 'www.punoftheday.com'
  };

  http.get(options, function (http_res) {
    // initialize the container for our data
    var data = "";

    // this event fires many times, each time collecting another piece of the response
    http_res.on("data", function (chunk) {
      // append this chunk to our growing `data` var
      data += chunk;
    });

    // this event fires *one* time, after all the `data` events/chunks have been gathered
    http_res.on("end", function () {
      console.log('done getting the pun of the day');
      // you can use res.send instead of console.log to output via express
      // console.log(data);
      var beforePun = '<div class="dropshadow1"><p>&#8220;';
      var afterPun = '&#8221;</p></div><br style="clear:left;" />';
      var pun = data.substring(data.indexOf(beforePun) + beforePun.length, data.indexOf(afterPun));
      callback(null,pun);
    });
  });
}
